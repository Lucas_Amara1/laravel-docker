# Laravel Docker

Projeto laravel com imagem para rodar em container docker, baseado na seguinte vídeo aula:

https://www.youtube.com/watch?v=XwBEsdpCgyU&t=9s

Para rodar o projeto e acessar em localhost:7000, basta dar o comando:

sudo docker-compose up -d --build

